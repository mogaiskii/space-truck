using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class LevelLoader : MonoBehaviour
{
    Shared shared;
    PlayerInputManager manager;

    public GameCamera gameCamera;

    // Start is called before the first frame update
    void Start()
    {
        manager = GetComponent<PlayerInputManager>();
        shared = FindObjectOfType<Shared>();
        PauseMenu.instance.Resume();

        foreach (PlayerInit item in shared.GetIns(Debug.isDebugBuild))
        {
            try
            {
                PlayerInput inp = manager.JoinPlayer(item.index, item.screenindex, item.scheme, item.devices);
                GameObject player = inp.gameObject;
                RegisterPlayer(player);


                player.GetComponent<PlayerController>().SetColor(item.color);
            }
            catch (System.Exception)
            {

            }
        }
    }

    public void RegisterPlayer(GameObject player)
    {
        gameCamera.AddPlayer(player);
    }

}
