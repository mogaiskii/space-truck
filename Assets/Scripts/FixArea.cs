using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixArea : MonoBehaviour
{
    public PlayerController Player;

    public Interactable targetInteractable;

    public DiseasterableItem targetedDiesiaster { get; protected set; }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        DiseasterableItem item = collision.gameObject.GetComponent<DiseasterableItem>();
        Interactable interactable = collision.gameObject.GetComponent<Interactable>();

        if (item || interactable)
        {
            if (targetedDiesiaster == null && targetInteractable == null)
            {
                if (item != null) EnterDiesaster(item);
                else if (interactable != null) EnterInteractable(interactable);
            } else
            {
                Vector2 v2pos = new Vector2(transform.position.x, transform.position.y);

                Vector3 oldPos = Vector3.zero;
                Vector3 newPos = Vector3.zero;

                if (item != null)
                {
                    newPos = item.transform.position;
                } else
                {
                    newPos = interactable.transform.position;
                }

                if (targetedDiesiaster != null)
                {
                    oldPos = targetedDiesiaster.transform.position;
                } else
                {
                    oldPos = targetInteractable.transform.position;
                }

                float collidedDistance = Vector2.Distance(
                    new Vector2(newPos.x, newPos.y),
                    v2pos
                    );
                float oldTargetedDistance = Vector2.Distance(
                    new Vector2(oldPos.x, oldPos.y),
                    v2pos
                    );

                if  (collidedDistance < oldTargetedDistance)
                {
                    if (item != null)
                    {
                        LeaveDiesiaster(targetedDiesiaster);
                        EnterDiesaster(item);
                    } else if (interactable != null)
                    {
                        LeaveInteractable(targetInteractable);
                        EnterInteractable(interactable);
                    }
                }
            }
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        DiseasterableItem item = collision.gameObject.GetComponent<DiseasterableItem>();
        if (item && item == targetedDiesiaster)
        {
            LeaveDiesiaster(item);
        }

        Interactable interactable = collision.gameObject.GetComponent<Interactable>();
        if (interactable && interactable == targetInteractable)
        {
            LeaveInteractable(interactable);
        }
    }


    void EnterDiesaster(DiseasterableItem item)
    {
        if (item == null) return;
        if (targetInteractable != null)
        {
            LeaveInteractable(targetInteractable);
        }
        targetedDiesiaster = item;
        targetedDiesiaster.Highlight();
    }

    void LeaveDiesiaster(DiseasterableItem item)
    {
        if (item == null) return;
        item.DisableHighlight();
        targetedDiesiaster = null;
    }

    void EnterInteractable(Interactable item)
    {
        if (item == null) return;
        if (targetedDiesiaster != null)
        {
            LeaveDiesiaster(targetedDiesiaster);
        }
        targetInteractable = item;
        targetInteractable.Highlight();
    }

    void LeaveInteractable(Interactable item)
    {
        if (item == null) return;
        item.DisableHighlight();
        targetInteractable = null;
    }
}
