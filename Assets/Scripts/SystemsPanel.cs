using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SystemsPanel : MonoBehaviour
{

    public static SystemsPanel instance;
    public GameObject SubSystemPrefab;
    List<SubSystemState> subSystems = new List<SubSystemState>();

    public GameObject UIlayer;

    public TextMeshProUGUI text;

    string ScoreFormat = "Score: {0}";
    float score = 0;


    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        instance = this;
    }

    public void AddScore(int adds)
    {
        score += adds;
    }

    public void Update()
    {
        score += Time.deltaTime;

        text.text = string.Format(ScoreFormat, ((int)score).ToString());
    }

    public void RegisterSystem(DiseasterableItem item)
    {
        GameObject newMonitor = Instantiate(SubSystemPrefab, UIlayer.transform);
        SubSystemState state = newMonitor.GetComponent<SubSystemState>();
        subSystems.Add(state);
        state.SetItem(item);
        state.LocatePosition(subSystems.Count);
    }
}
