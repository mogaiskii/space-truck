using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameCamera : MonoBehaviour
{
    public GameObject spaceship;
    List<GameObject> players = new List<GameObject>();
    public float smoothSpeed = 2f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void AddPlayer(GameObject player)
    {
        players.Add(player);
    }

    Vector3 GetCentroid()
    {
        Vector3 centroid = new Vector3();
        centroid += spaceship.transform.position;
        foreach (var player in players)
        {
            centroid += player.transform.position;
        }
        centroid /= (1 + players.Count);
        return centroid;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 target = GetCentroid();
        Vector3 desiredPosition = new Vector3(target.x, target.y, transform.position.z);
        transform.position = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.deltaTime);
    }
}
