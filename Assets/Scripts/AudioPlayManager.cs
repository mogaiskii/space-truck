using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


[System.Serializable]
public class Sound
{
    public string name;
    public AudioClip clip;

    [Range(0, 1f)]
    public float volume;

    public AudioSource source;

    public bool loop;
    public bool from_start;

    public string tag;
    public bool enabled = true;
}

public class AudioPlayManager : MonoBehaviour
{
    [SerializeField]
    List<Sound> sounds = new List<Sound>();

    static AudioPlayManager instance;
    static bool SHARED_MODE = false;

    // Start is called before the first frame update
    void Awake()
    {
        if (SHARED_MODE)
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(this);
                return;
            }
        }

        foreach (var sound in sounds)
        {
            Load(sound);
        }
    }

    private void Start()
    {
        foreach (var sound in sounds.FindAll(s => s.from_start))
        {
            _Play(sound);
        }
    }

    void Load(Sound sound)
    {
        if (sound.source != null)
        {
            Destroy(sound.source);
        }
        sound.source = gameObject.AddComponent<AudioSource>();
        sound.source.clip = sound.clip;
        sound.source.volume = sound.volume;
        sound.source.loop = sound.loop;
    }

    public void Play(string name)
    {
        Sound sound = sounds.Find(s => s.name == name);
        _Play(sound);
    }


    private void _Play(Sound sound)
    {
        if (sound == null)
        {
            Debug.LogWarning("Sound not found " + name);
            return;
        }
        if (sound.enabled)
            sound.source.Play();
    }

    public void SwitchByTag(string tag)
    {
        foreach (var sound in sounds.FindAll(s => s.tag == tag))
        {
            Switch(sound);
        }
    }

    private void Switch(Sound sound)
    {
        if (sound.enabled)
        {
            sound.enabled = false;
            sound.source.volume = 0;
        }
        else
        {
            sound.enabled = true;
            sound.source.volume = sound.volume;
        }
    }
}
