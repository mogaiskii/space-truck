using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HighlightFix : MonoBehaviour
{

    // Update is called once per frame
    void Update()
    {
        if (gameObject.transform.childCount > 0)
        {

            foreach (Transform child in gameObject.transform)
            {
                Destroy(child.gameObject);
            }
        }
    }
}
