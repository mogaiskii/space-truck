using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum StuffType
{
    box,
    detail
}


public class Draggable : MonoBehaviour
{
    public StuffType type;

    PlayerController player;


    public void GetToPlayer(PlayerController player)
    {
        player.GiveItem(this);
    }

    public void OnStartDrag(PlayerController player)
    {
        this.player = player;
        Interactable interactable = gameObject.GetComponent<Interactable>();
        if (interactable != null) {
            interactable.enabled = false;
        }
    }

    public void StopDrag(Vector3 position)
    {
        gameObject.transform.position = position;
        Interactable interactable = gameObject.GetComponent<Interactable>();
        if (interactable != null)
        {
            interactable.enabled = true;
        }
        player = null;
        transform.parent = null;
    }


}
