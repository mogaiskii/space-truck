using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HackReload : MonoBehaviour
{
    float load_treshhold = 1f;
    public string scene;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;
        Resources.UnloadUnusedAssets();
        System.GC.Collect();
    }

    // Update is called once per frame
    void Update()
    {
        load_treshhold -= Time.deltaTime;
        if (load_treshhold <= 0)
        {
            SceneManager.LoadScene(scene);
        }
    }
}
