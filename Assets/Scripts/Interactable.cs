using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


[Serializable]
public class InteractionEvent : UnityEvent<PlayerController> { }


public class Interactable : MonoBehaviour
{
    bool JustACopy = false;


    SpriteRenderer m_renderer;

    bool highlighted = false;
    GameObject highlighter; SpriteRenderer highlighter_render;
    float highlightState = 0; bool hightlightIncrease = true;
    float highlightStateB = 1; bool hightlightIncreaseB = false;
    float hightlightSpeed = 1.3f;

    private void Awake()
    {
        Transform parent = gameObject.transform.parent;
        if (parent != null && parent.GetComponent<DiseasterableItem>() != null)
        {
            JustACopy = true;
            Destroy(gameObject.GetComponent<Collider2D>());
            Destroy(this);
            foreach (Transform child in gameObject.transform)
            {
                Destroy(child.gameObject);
            }
            return;
        }
    }

    protected void Start()
    {
        Transform parent = gameObject.transform.parent;
        if (parent != null && parent.GetComponent<DiseasterableItem>() != null)
        {
            JustACopy = true;
            Destroy(this);
            return;
        }

        m_renderer = GetComponent<SpriteRenderer>();
    }

    public InteractionEvent OnInteraction;
    public InteractionEvent OnStopInteraction;

    public void Interact(PlayerController player)
    {
        OnInteraction?.Invoke(player);
    }

    public void StopInteraction(PlayerController player)
    {
        Debug.Log("Stopinteraction");
        OnStopInteraction?.Invoke(player);
    }


    public void Highlight()
    {
        if (highlighted) return;
        highlighted = true;
        highlighter = Instantiate(this.gameObject);
        highlighter.transform.parent = this.transform;
        highlighter.transform.localPosition = new Vector3(0, 0, 1);
        highlighter.transform.localScale *= 1.1f;
        highlighter_render = highlighter.GetComponent<SpriteRenderer>();
        highlighter_render.color = Color.yellow;
        Destroy(highlighter.GetComponent<Interactable>());
        foreach (Transform child in highlighter.transform)
        {
            Destroy(child.gameObject);
        }
        highlighter.AddComponent<HighlightFix>();
    }
    public void DisableHighlight()
    {
        highlighted = false;
        Destroy(highlighter);
    }


    private void Update()
    {

        if (highlighted)
        {
            if (hightlightIncrease)
            {
                highlightState += hightlightSpeed * Time.deltaTime;
                if (highlightState >= 1)
                {
                    highlightState = 1; hightlightIncrease = false;
                }
            }
            else
            {
                highlightState -= hightlightSpeed * Time.deltaTime;
                if (highlightState <= 0)
                {
                    highlightState = 0; hightlightIncrease = true;
                }
            }
            if (hightlightIncreaseB)
            {
                highlightStateB += hightlightSpeed * Time.deltaTime;
                if (highlightStateB >= 1)
                {
                    highlightStateB = 1; hightlightIncreaseB = false;
                }
            }
            else
            {
                highlightStateB -= hightlightSpeed * Time.deltaTime;
                if (highlightStateB <= 0)
                {
                    highlightStateB = 0; hightlightIncreaseB = true;
                }
            }
            highlighter_render.color = new Color(1 - highlightState, 1 - highlightState, 1 - highlightStateB);
        }
    }
}
