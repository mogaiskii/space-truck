using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


public enum DiseasterableItemState
{
    OK,
    FIX,
    FAIL,
    WARN,
    DEAD
}
public class DiseasterableItem : MonoBehaviour
{
    [Range(0, 1f)]
    public float DiseasterChance = 0.5f;

    public float DiseasterCooldown = 15f;  // seconds
    public float DiseasterStartCooldownDiff = 6f; // seconds
    public float DiseasterOnFullCooldown = 30f; // seconds
    bool wasFixedFull = true;
    bool wasEvenDiseastered = false;

    private float LastDiseasterClock = 0f;

    private float LastDiseasterCheckClock = 0f;

    DiseastersManager manager;

    public bool inDiseaster = false;

    float redState = 0; bool redStateIncrease = true;
    public float redStateSpeed = 1f;


    bool JustACopy = false;


    SpriteRenderer m_renderer;

    bool highlighted = false;
    GameObject highlighter; SpriteRenderer highlighter_render;
    float highlightState = 0; bool hightlightIncrease = true;
    float highlightStateB = 1; bool hightlightIncreaseB = false;
    float hightlightSpeed = 1.3f;

    public float FixTime = 1f;
    public float HotFixTime = 0.3f;
    public float FixClock = 0f;
    public bool IsRepairing = false;
    RepairLine Fixer;

    public string Title;
    public float MaxResource = 100f;
    public float resource = 100f;
    public float AddResourceOnHotfix = 3f;
    public float AddResourceOnRepair = 7f;
    public float AddResourceOnRepairWithDetail = 15f;

    bool danger = false;

    GameObject detail;
    bool DetailLoaded = false;


    private float RecheckTime()
    {
        if (manager == null) return 1.5f;
        return Mathf.Abs(1 + (Mathf.Log(manager.DieseastersCount + 0.1f)));
    }


    public DiseasterableItemState GetState()
    {
        if (IsRepairing) return DiseasterableItemState.FIX;
        if (inDiseaster && danger) return DiseasterableItemState.WARN;
        if (inDiseaster) return DiseasterableItemState.FAIL;
        if (resource <= 0) return DiseasterableItemState.DEAD;
        return DiseasterableItemState.OK;
    }


    public void LoadDetail()
    {
        DetailLoaded = true;
    }

    public int ResourcePercent()
    {
        return (int)Mathf.Ceil(resource / MaxResource * 100);
    }

    public Sprite Icon()
    {
        return m_renderer.sprite;
    }


    private void Awake()
    {
        Transform parent = gameObject.transform.parent;
        if (parent != null && parent.GetComponent<DiseasterableItem>() != null)
        {
            JustACopy = true;
            Destroy(gameObject.GetComponent<Collider2D>());
            Destroy(this);
            foreach (Transform child in gameObject.transform)
            {
                Destroy(child.gameObject);
            }
            return;
        }
    }

    protected void Start()
    {
        Transform parent = gameObject.transform.parent;
        if (parent != null && parent.GetComponent<DiseasterableItem>() != null)
        {
            JustACopy = true;
            Destroy(this);
            return;
        }

        manager = DiseastersManager.instance;
        m_renderer = GetComponent<SpriteRenderer>();
        SystemsPanel.instance.RegisterSystem(this);
    }


    public void Highlight()
    {
        if (highlighted) return;
        highlighted = true;
        highlighter = Instantiate(this.gameObject);
        highlighter.transform.parent = this.transform;
        highlighter.transform.localPosition = new Vector3(0, 0, 1);
        highlighter.transform.localScale *= 1.1f;
        highlighter_render = highlighter.GetComponent<SpriteRenderer>();
        highlighter_render.color = Color.yellow;

        Destroy(highlighter.GetComponent<DiseasterableItem>());
        foreach (Transform child in highlighter.transform)
        {
            Destroy(child.gameObject);
        }
        highlighter.AddComponent<HighlightFix>();
    }
    public void DisableHighlight()
    {
        highlighted = false;
        Destroy(highlighter);
    }

    float Clock()
    {
        float clockTo = DiseasterCooldown;
        if (!wasEvenDiseastered) clockTo = DiseasterStartCooldownDiff;
        else if (wasFixedFull) clockTo = DiseasterOnFullCooldown;
        float normalClock = clockTo - LastDiseasterClock;

        float innerClock = RecheckTime() - LastDiseasterCheckClock;

        float globalClock = 0f;
        if (manager != null)
            globalClock = manager.disaster_global_cooldown;

        if (normalClock <= 0)
        {
            if (globalClock > innerClock) return globalClock;
            return innerClock;
        } else
        {
            if (globalClock >= normalClock) return globalClock;
            return normalClock;
        }
    }
    private bool ItsTime()
    {
        if (manager.SinceAllFixed > 0.5f && wasEvenDiseastered)
        {
            return true;
        }
        return Clock() <= 0;
    }
    private bool ItShouldHappen()
    {
        if (manager.SinceAllFixed > 1.5f && manager.DieseastersCount == 0)
        {
            return true;
            float targ = DiseasterChance;
            if (1 - DiseasterChance > DiseasterChance) targ = 1 - DiseasterChance;
            return Random.Range(0f, 1f) > 0.4f;
        }

        float mul = (manager.DieseastersCount * manager.DieseastersCount / 8);
        if (mul < 0.85f) mul = 0.85f;
        if (mul > 3) mul = 3;
        float target = DiseasterChance / mul;
        float rand = Random.Range(0, 1f);
        return rand <= target;
    }

    // void OnDrawGizmos()
    //{
    //   Handles.Label(transform.position, Clock().ToString() + "s");
    // }

    public Color color()
    {
        return m_renderer.color;
    }

    private void Update()
    {
        if (PauseMenu.GameIsPaused) return;

        LastDiseasterClock += Time.deltaTime;
        LastDiseasterCheckClock += Time.deltaTime;

        if (DetailLoaded && detail == null && manager.DetailEmptyPrefab != null)
        {
            detail = Instantiate(manager.DetailEmptyPrefab, gameObject.transform);
            float offs = detail.GetComponent<SpriteRenderer>().bounds.size.y / 2;
            detail.transform.localPosition = new Vector3(0, offs, -1);
        }

        if (inDiseaster)
        {
            if (redStateIncrease)
            {
                redState += redStateSpeed * Time.deltaTime;
                if (redState >= 1)
                {
                    redState = 1; redStateIncrease = false;
                }
            }
            else
            {
                redState -= redStateSpeed * Time.deltaTime;
                if (redState <= 0)
                {
                    redState = 0; redStateIncrease = true;
                }
            }
            m_renderer.color = new Color(1, 1 - redState, 1 - redState);

            if (IsRepairing)
            {
                FixClock += Time.deltaTime;
                float state = FixClock / FixTime;
                if (state > 1) state = 1;
                Fixer.ChangeState(state);
            } else
            {
                resource -= Time.deltaTime;
            }
        }
        else if (ItsTime())
        {
            if (ItShouldHappen() || manager.TotalDieseastersCount == 0)
            {
                Diseaster();
            }
            else
            {
            }
            LastDiseasterCheckClock = 0f;
        }

        if (resource < MaxResource / 10)
        {
            danger = true;
        }
        if (resource <= 0)
        {
            GameOverPanel.instance.GameOver();
        }

        if (highlighted)
        {
            if (hightlightIncrease)
            {
                highlightState += hightlightSpeed * Time.deltaTime;
                if (highlightState >= 1)
                {
                    highlightState = 1; hightlightIncrease = false;
                }
            }
            else
            {
                highlightState -= hightlightSpeed * Time.deltaTime;
                if (highlightState <= 0)
                {
                    highlightState = 0; hightlightIncrease = true;
                }
            }
            if (hightlightIncreaseB)
            {
                highlightStateB += hightlightSpeed * Time.deltaTime;
                if (highlightStateB >= 1)
                {
                    highlightStateB = 1; hightlightIncreaseB = false;
                }
            }
            else
            {
                highlightStateB -= hightlightSpeed * Time.deltaTime;
                if (highlightStateB <= 0)
                {
                    highlightStateB = 0; hightlightIncreaseB = true;
                }
            }
            highlighter_render.color = new Color(1 - highlightState, 1 - highlightState, 1 - highlightStateB);
        }
    }

    public void StartFix()
    {
        IsRepairing = true;
        GameObject FixerObj = Instantiate(manager.RepairLinePrefab, gameObject.transform);
        FixerObj.transform.localPosition = new Vector3(0, 0, 0);
        Fixer = FixerObj.GetComponent<RepairLine>();
        Fixer.SetTresholds(HotFixTime / FixTime);
    }
    public void AbortFix()
    {
        if (!IsRepairing) return;
        IsRepairing = false;
        if (FixClock > FixTime)
        {
            Fix(true);
        }
        else if (FixClock > HotFixTime)
        {
            Fix(false);
        }
        FixClock = 0f;
        Destroy(Fixer);
        Destroy(Fixer.gameObject);
    }


    public void Fix(bool full = false)
    {
        inDiseaster = false;
        LastDiseasterClock = 0f;
        wasFixedFull = full;
        m_renderer.color = Color.white;
        StopConsequences();

        if (full)
        {
            if (DetailLoaded)
            {
                SystemsPanel.instance.AddScore((int)AddResourceOnRepairWithDetail);
                resource += AddResourceOnRepairWithDetail;
                Destroy(detail);
                detail = null;
                DetailLoaded = false;
            }
            else
            {
                SystemsPanel.instance.AddScore((int)AddResourceOnRepair);
                resource += AddResourceOnRepair;
            }
        } else
        {
            resource += AddResourceOnHotfix;
            SystemsPanel.instance.AddScore((int)AddResourceOnHotfix);
        }
        if (resource > MaxResource) resource = MaxResource;
    }

    public void Diseaster()
    {
        LastDiseasterClock = 0f;
        manager.DisasterHappened();
        inDiseaster = true;
        redStateIncrease = true;
        redState = 0;
        wasEvenDiseastered = true;
        Consequences();
    }

    protected virtual void Consequences()
    {

    }

    protected virtual void StopConsequences()
    {

    }
}
