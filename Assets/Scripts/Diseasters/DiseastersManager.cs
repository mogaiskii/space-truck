using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiseastersManager : MonoBehaviour
{

    public float disaster_global_cooldown = 0f;

    public float DisasterCooldown = 5f;  bool coundownmark = true;

    public static DiseastersManager instance;

    bool ThingsHappened = false;

    public int DieseastersCount = 0;
    public int TotalDieseastersCount { get; private set; } = 0;

    public float SinceAllFixed = 0f;

    public GameObject RepairLinePrefab;
    public GameObject DetailEmptyPrefab;

    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
            Destroy(this.gameObject);
            return;
        }

        instance = this;
    }


    public void DisasterHappened()
    {
        disaster_global_cooldown = DisasterCooldown;
        DieseastersCount += 1;
        ThingsHappened = true;
        TotalDieseastersCount += 1;
        SinceAllFixed = 0f;
    }
    public void DiseasterFixed()
    {
        if (DieseastersCount > 0)
        {

            DieseastersCount -= 1;

        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (PauseMenu.GameIsPaused) return;

        if (DieseastersCount == 0)
        {
            SinceAllFixed += Time.deltaTime;
        }

        if (ThingsHappened)
        {
            //disaster_global_cooldown = (Mathf.Sqrt(DisasterCooldown * DieseastersCount) + DieseastersCount * 4 / 3) / Mathf.Sqrt(10 - DieseastersCount) + Mathf.Pow(3, Mathf.Log(DieseastersCount * 1.2f + 0.1f));
            //disaster_global_cooldown = disaster_global_cooldown / (Mathf.Log10(TotalDieseastersCount + 10));
            disaster_global_cooldown = DisasterCooldown / (Mathf.Log10(TotalDieseastersCount + 10));
            ThingsHappened = false; coundownmark = false;
        }
        if (DieseastersCount == 0 && !coundownmark)
        {
            disaster_global_cooldown = DisasterCooldown / Mathf.Log10(TotalDieseastersCount + 10);
            coundownmark = true;
        }
        if (disaster_global_cooldown > 0)
        {
            disaster_global_cooldown -= Time.deltaTime;
        }
    }
}
