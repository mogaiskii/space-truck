using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    Vector2 move;

    Rigidbody2D m_rigidbody;
    SpriteRenderer m_renderer;


    public float speed = 1f; bool stopped = false;

    public FixArea area;

    bool InInteraction = false;
    Interactable interactableItem;
    DiseasterableItem repairingItem;

    public PlayerAvatarHack AvatarHack;

    public void OnMoveChange(InputAction.CallbackContext ctx)
    {
        move = ctx.ReadValue<Vector2>();

        if (move.sqrMagnitude > 0 && !stopped && !InInteraction )
        {
            AvatarHack.Play("Walk");
            AvatarHack.Flip(move.x <= 0);
        } else if (!InInteraction)
        {
            AvatarHack.Play("Idle");
        }
    }

    internal void SetColor(Color color)
    {
        AvatarHack.SetColor(color);
    }

    public void AbortInteraction()
    {
        InInteraction = false;
        if (repairingItem != null)
        {
            repairingItem.AbortFix();
        }
        repairingItem = null;
        if (interactableItem != null)
        {
            Debug.Log("interactableItem.StopInteraction");
            interactableItem.StopInteraction(this);
        }
        interactableItem = null;
        AvatarHack.Play("Idle");
    }

    public void StopMove()
    {
        stopped = true;
    }
    public void ResumeMove()
    {
        stopped = false;
    }

    public void OnActionChange(InputAction.CallbackContext ctx)
    {
        if (stopped) return;

        bool wasIt = ctx.action.triggered;

        if (wasIt && draggingItem != null)
        {
            if (!InInteraction && area.targetInteractable != null && area.targetInteractable.GetComponent<DPrinter>() != null && draggingItem.type == StuffType.box)
            {
                area.targetInteractable.GetComponent<DPrinter>().LoadBox();
                Destroy(draggingItem.gameObject);
                draggingItem = null;
            } else if (!InInteraction && area.targetedDiesiaster != null)
            {
                Destroy(draggingItem.gameObject);
                draggingItem = null;
                area.targetedDiesiaster.LoadDetail();
            } else if (!InInteraction && area.targetInteractable != null && area.targetInteractable.GetComponent<LockerOpen>() != null)
            {
                AvatarHack.Play("Idle");
                interactableItem = area.targetInteractable;
                area.targetInteractable.Interact(this);
                InInteraction = true;
            } else
            {
                draggingItem.StopDrag(transform.position);
                draggingItem = null;
            }
        } else if (!InInteraction && wasIt && area.targetedDiesiaster != null && area.targetedDiesiaster.inDiseaster)
        {
            AvatarHack.Play("Action");
            area.targetedDiesiaster.StartFix();
            InInteraction = true;
            repairingItem = area.targetedDiesiaster;
        } else if (!InInteraction && wasIt && area.targetInteractable != null)
        {
            AvatarHack.Play("Action");
            interactableItem = area.targetInteractable;
            area.targetInteractable.Interact(this);
            InInteraction = true;
        }
        else if (!wasIt && InInteraction)
        {
            AvatarHack.Play("Idle");
            AbortInteraction();
        }
    }


    public Draggable draggingItem { get; private set; }

    public void GiveItem(Draggable item)
    {
        //throw new NotImplementedException();
        draggingItem = item;
        item.transform.parent = gameObject.transform;
        item.transform.localPosition = new Vector3(0, m_renderer.bounds.size.y/2, item.transform.localPosition.z);
    }

    public void Teleport(Vector3 position)
    {
        gameObject.transform.position = position;
    }

    public void OnPauseChange(InputAction.CallbackContext ctx)
    {
        bool wasIt = ctx.action.triggered;
        PauseMenu.instance.TogglePause();
    }

    private void Start()
    {
        move = Vector2.zero;
        m_rigidbody = GetComponent<Rigidbody2D>();
        m_renderer = GetComponent<SpriteRenderer>();
    }

    void FixedUpdate()
    {
        if (PauseMenu.GameIsPaused) return;

        if (stopped)
        {
            m_rigidbody.velocity = Vector2.zero;
        }
        else if (!InInteraction)
        {
            Vector2 c_speed = move * speed;
            m_rigidbody.velocity = c_speed;

        } else if (InInteraction)
        {
            m_rigidbody.velocity = Vector2.zero;
        }

    }

}
