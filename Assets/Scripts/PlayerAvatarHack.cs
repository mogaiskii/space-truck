using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAvatarHack : MonoBehaviour
{
    Animator m_anim;

    // Start is called before the first frame update
    void Start()
    {
        m_anim = GetComponent<Animator>();
    }

    public void Play(string name)
    {
        m_anim.Play(name);
    }


    bool lookLeft = true;
    public void Flip(bool bleft)
    {
        if (lookLeft != bleft)
        {
            transform.localScale = new Vector3(bleft ? 1 : -1, 1, 1);
            lookLeft = bleft;
        }
    }

    internal void SetColor(Color color)
    {
        Debug.Log(color);
        foreach (Transform item in gameObject.transform)
        {
            item.gameObject.GetComponent<SpriteRenderer>().color = color;
        }
    }
}
