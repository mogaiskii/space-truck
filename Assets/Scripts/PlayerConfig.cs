using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerConfig
{
    public PlayerInput Input;
    public int PlayerIndex;
    public bool isReady;
    public Color color;
    public PlayerSelection selector;

    public PlayerConfig(PlayerInput input)
    {
        Input = input;
        PlayerIndex = input.playerIndex;
    }

}
