using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class PlayerManager : MonoBehaviour
{

    List<PlayerConfig> configs = new List<PlayerConfig>();
    public int MaxPlayers = 4;

    public bool inGame = false;

    public string GameSceen;

    public GameObject playerPrefab;
    public GameObject guide;

    public static PlayerManager instance { get; private set; }

    PlayerInputManager inputManager;

    public void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
            return;

        }


        instance = this;
        inputManager = GetComponent<PlayerInputManager>();
    }

    public void OnPlayerJoin(PlayerInput input)
    {
        Debug.Log("PLAYERJOIN");
        if (!inGame && configs.Count < MaxPlayers && configs.FindIndex(c => c.PlayerIndex == input.playerIndex) == -1)
        {
            input.transform.SetParent(transform);
            PlayerConfig c = new PlayerConfig(input);
            switch (input.playerIndex)
            {
                case 0:
                    ColorUtility.TryParseHtmlString("#99c45a", out c.color);
                    break;
                case 1:
                    ColorUtility.TryParseHtmlString("#b62dff", out c.color);
                    break;
                case 2:
                    ColorUtility.TryParseHtmlString("#ef7a45", out c.color);
                    break;
                case 3:
                    ColorUtility.TryParseHtmlString("#fff573", out c.color);
                    break;
                default:
                    break;
            }
            configs.Add(c);
            Shared.instance.AddPlayer(c);
            Debug.Log("PLAYERJOIN CONFIRM");
        }
    }
    public void OnPlayerLost(PlayerInput input)
    {
        // pause game with message??
        Debug.Log("Player lost #" + input.ToString());
    }

    public void OnPlayerLeft(PlayerInput input)
    {

    }


    public PlayerConfig GetConfig(PlayerInput input)
    {
        return configs.Find(c => c.Input.playerIndex == input.playerIndex);
    }


    public void SetPlayerColor(int index, Color color)
    {
        configs[index].color = color;
    }
    bool ready = false;
    public void SetPlayerReady(int index, bool state=true)
    {
        configs[index].isReady = state;
        if (!ready && configs.TrueForAll(c => c.isReady))
        {
            OnAllReady();
        }
        else if (ready) StartGame();
    }

    void StartGame()
    {


        SceneManager.LoadScene(GameSceen);
    }
    public void OnAllReady()
    {
        if (inGame) return;

        try
        {
            //selectors.ForEach(i => Destroy(i.gameObject));
        }
        catch (System.Exception e)
        {
            Debug.LogError(e);
        }

        inputManager.playerPrefab = playerPrefab;
        inputManager.joinBehavior = PlayerJoinBehavior.JoinPlayersManually;
        ready = true;
        guide.SetActive(true);
    }

}
