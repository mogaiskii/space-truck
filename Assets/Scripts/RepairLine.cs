using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepairLine : MonoBehaviour
{
    [Range(0,1f)]
    public float state = 0f;

    public GameObject StateLine;
    public SpriteRenderer StateLineRenderer;

    public GameObject hotfixLine;
    public SpriteRenderer hotfixLineRenderer;

    [Range(0, 1f)]
    public float hotfixTreshold = 0.3f;

    public Color progressColor = Color.blue;
    public Color hotfixColor = Color.yellow;
    public Color FullfixColor = Color.green;

    private void Awake()
    {
        if (StateLineRenderer == null)
        {
            StateLineRenderer = StateLine.GetComponent<SpriteRenderer>();
        }
    }

    private void OnValidate()
    {
        if (StateLineRenderer == null )
        {
            StateLineRenderer = StateLine.GetComponent<SpriteRenderer>();
        }
        OnStateChanged();
        SetTresholds(hotfixTreshold);
    }

    public void SetTresholds(float hotfix)
    {
        hotfixTreshold = hotfix;

        hotfixLine.transform.localScale = new Vector3(hotfix, hotfixLine.transform.localScale.y, hotfixLine.transform.localScale.z);
        float width = hotfixLineRenderer.sprite.rect.width / hotfixLineRenderer.sprite.pixelsPerUnit / 2;
        hotfixLine.transform.localPosition = new Vector3(-1 * (1 - hotfix) * width + 0.05f, hotfixLine.transform.localPosition.y, hotfixLine.transform.localPosition.z);

    }

    public void ChangeState(float state)
    {
        this.state = state;
        OnStateChanged();
    }

    private void OnStateChanged()
    {
        StateLine.transform.localScale = new Vector3(state, StateLine.transform.localScale.y, StateLine.transform.localScale.z);
        float width = StateLineRenderer.sprite.rect.width / StateLineRenderer.sprite.pixelsPerUnit / 2;
        StateLine.transform.localPosition = new Vector3(-1 * (1 - state) * width + 0.05f, StateLine.transform.localPosition.y, StateLine.transform.localPosition.z);

        if (state >= 1f)
        {
            StateLineRenderer.color = FullfixColor;
        }  else if (state >= hotfixTreshold)
        {
            StateLineRenderer.color = hotfixColor;
        } else
        {
            StateLineRenderer.color = progressColor;
        }
    }
}
