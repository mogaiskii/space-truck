using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverPanel : MonoBehaviour
{
    public GameObject GameOverMenuUI;
    public UnityEngine.UI.Button firstButton;

    public static GameOverPanel instance;
    public string hackScene;


    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        instance = this;
    }

    public void Retry()
    {
        Time.timeScale = 1f;
        SceneManager.UnloadScene(SceneManager.GetActiveScene().buildIndex);
        SceneManager.LoadScene(hackScene);
    }

    public void GameOver()
    {
        GameOverMenuUI.SetActive(true);
        PauseMenu.GameIsPaused = true;
        Time.timeScale = 0f;
        firstButton.Select();
    }

    public void OnContinuePlayingClick()
    {
        Retry();
    }

    public void OnExitClick()
    {
        Application.Quit();
    }
}
