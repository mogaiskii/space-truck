using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clouds : MonoBehaviour
{

    public List<SpriteRenderer> cloud;

    bool increasing = false;
    bool decreasing = false;

    // Start is called before the first frame update
    void Start()
    {
        foreach (SpriteRenderer item in cloud)
        {
            item.color = new Color(1, 1, 1, 0);
        }
    }

    public void DoAnimation()
    {
        increasing = true;
        decreasing = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (increasing)
        {
            foreach (SpriteRenderer item in cloud)
            {
                float alpha = item.color.a;
                alpha += Random.Range(0.01f, 1f) * Time.deltaTime;
                if (alpha >= 1)
                {
                    increasing = false;
                    decreasing = true;
                    alpha = 1;
                }
                item.color = new Color(1, 1, 1, alpha);
            }
        } else if (decreasing)
        {
            foreach (SpriteRenderer item in cloud)
            {
                float alpha = item.color.a;
                alpha -= Random.Range(0.01f, 1f) * Time.deltaTime;
                if (alpha <= 0)
                {
                    decreasing = false;
                    alpha = 0;
                }
                item.color = new Color(1, 1, 1, alpha);
            }
        }
    }
}
