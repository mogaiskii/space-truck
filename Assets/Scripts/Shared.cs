using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


[System.Serializable]
public class PlayerInit
{
    public int index;
    public int screenindex;
    public string scheme;
    public InputDevice[] devices;
    public Color color { get
        {
            return new Color(r, g, b, 1);
        } set {
            r = value.r; g = value.g; b = value.b;
        } }
    float r = 1; float g = 1; float b = 1;
    

    public PlayerInit(int index, int screenindex, string scheme, InputDevice[] devices, Color color)
    {
        Debug.Log(color);
        this.index = index; this.scheme = scheme; this.screenindex = screenindex; this.devices = devices; this.color = color;
    }
}

[System.Serializable]
public class Shared : MonoBehaviour
{

    public List<PlayerInit> ins = new List<PlayerInit>();


    public static Shared instance;

    public List<PlayerConfig> confs = new List<PlayerConfig>();

    public int a = 1;
    // Start is called before the first frame update
    private void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
            Destroy(this.gameObject);
            return;
        }
        DontDestroyOnLoad(this.gameObject);
        instance = this;

    }

    public void AddPlayer(PlayerConfig c)
    {
        this.ins.Add(new PlayerInit(c.PlayerIndex, c.Input.splitScreenIndex, c.Input.currentControlScheme, c.Input.devices.ToArray(), c.color));
        Debug.Log((c.Input.currentControlScheme));
    }

    public List<PlayerInit> GetIns(bool allow_fakes=false)
    {
        List<PlayerInit> res = new List<PlayerInit>();

        foreach (var item in ins)
        {
            res.Add(item);
        }
        if (allow_fakes && res.Count == 0)
        {
            Debug.Log("Fake player");
            PlayerInit i = new PlayerInit(1, 1, "keyboard", new InputDevice[] { InputSystem.devices[0] }, Color.white);
            res.Add(i);
        }

        return res;
    }

    private void Start()
    {
        
    }

    private void Update()
    {
    }

}
