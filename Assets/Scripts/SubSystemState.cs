using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SubSystemState : MonoBehaviour
{

    public DiseasterableItem original;

    public TextMeshProUGUI Title;
    public TextMeshProUGUI State;
    public TextMeshProUGUI Resource;
    public Image Icon;
    public Image Background;


    string StateFormat = "State: {0}";
    string ResourceFormat = "resource:{0}%";

    DiseasterableItemState state;
    Color color = Color.white;

    bool GoingRed = false;

    // Start is called before the first frame update
    void Start()
    {
        Rewrite();
    }

    public void SetItem(DiseasterableItem item)
    {
        original = item;
        Rewrite();
    }
    void Rewrite()
    {
        Title.text = original.Title;
        state = original.GetState();
        State.text = string.Format(StateFormat, state.ToString());
        Resource.text = string.Format(ResourceFormat, original.ResourcePercent().ToString());
        Icon.sprite = original.Icon();
    }

    public void LocatePosition(int number)
    {
        RectTransform rectTransform = gameObject.GetComponent<RectTransform>();
        float basePos = 150;
        float baseSep = 10;
        float scaled = (basePos + baseSep) * Camera.main.scaledPixelWidth / ((basePos + baseSep) * 9);
        float scaleOpt = (basePos + baseSep) / scaled;
        rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x + scaled * (number-1) - 5, rectTransform.anchoredPosition.y);
        rectTransform.localScale = new Vector3(scaleOpt, rectTransform.localScale.y, rectTransform.localScale.z);
    }

    void OnStateChanged(DiseasterableItemState oldState, DiseasterableItemState newState)
    {
        if (newState == DiseasterableItemState.DEAD)
        {
            color = Color.black;
            GoingRed = false;
        }
        else if (newState == DiseasterableItemState.OK)
        {
            color = Color.cyan;
            GoingRed = false;
        }
        else if (newState == DiseasterableItemState.FIX)
        {
            GoingRed = false;
            color = new Color(255, 150, 150);
        }
        else if (newState == DiseasterableItemState.FAIL)
        {
            GoingRed = true;
        } else if (newState == DiseasterableItemState.WARN)
        {
            GoingRed = false;
            color = Color.red;
        }
    }
    // Update is called once per frame
    void Update()
    {
        DiseasterableItemState newState = original.GetState();
        if (state != newState)
        {
            OnStateChanged(state, newState);
        }
        state = newState;
        State.text = string.Format(StateFormat, state.ToString());
        Resource.text = string.Format(ResourceFormat, original.ResourcePercent().ToString());

        if (GoingRed)
        {
            color = original.color();
        }

        Background.color = color;
    }
}
