using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockerOpen : MonoBehaviour
{
    public float cooldown = 5f;
    float cooldownClock = 0f;

    public float Timer = 2.5f;
    float timerClock = 0f;

    public Clouds clouds;

    PlayerController interactingWith;

    public Transform teleport;

    public LockerOpen nextLocker;

    public void OnOpen(PlayerController player)
    {
        if (cooldownClock >= 0) return;
        clouds.DoAnimation();
        cooldownClock = cooldown;
        player.AbortInteraction();
        //player.StopMove();
        interactingWith = player;
        timerClock = Timer;
    }

    private void Update()
    {
        if (cooldownClock >= 0)
        {
            cooldownClock -= Time.deltaTime;
        }

        if (interactingWith != null)
        {
            timerClock -= Time.deltaTime;
            if (timerClock <= 0)
            {
                interactingWith.Teleport(teleport.position);
                //interactingWith.ResumeMove();
                interactingWith = null;
                nextLocker.gameObject.SetActive(true);
                gameObject.SetActive(false);
            }
        }

    }
}
