using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DPrinter : MonoBehaviour
{
    PlayerController playerToInteract;

    RepairLine line;

    public GameObject RepairLinePrefab;
    public GameObject DetailPrefab;

    public float TimeToPrint = 5f;
    float PrintClock = 0f;
    bool isPrinting = false;
    int boxesCount = 1;

    SpriteRenderer m_renderer;

    GameObject box;
    public GameObject PrefabEmptyBox;


    private void Start()
    {
        m_renderer = GetComponent<SpriteRenderer>();
    }

    

    public void OnInteract(PlayerController player)
    {

        if (playerToInteract != null  || boxesCount <= 0)
        {
            player.AbortInteraction();
            return;
        }

        playerToInteract = player;
        StartPrinting();
    }

    void StartPrinting()
    {
        Debug.Log("StartPring");
        GameObject FixerObj = Instantiate(RepairLinePrefab, gameObject.transform);
        FixerObj.transform.localPosition = new Vector3(0, 0, 0);
        line = FixerObj.GetComponent<RepairLine>();
        line.SetTresholds(0f);
        isPrinting = true;
        PrintClock = 0f;
    }

    public void Update()
    {
        if (isPrinting)
        {
            PrintClock += Time.deltaTime;
            float state = PrintClock / TimeToPrint;
            if (state > 1) {
                state = 1;
                if (PrintClock > TimeToPrint + 1f)
                {
                    //playerToInteract.AbortInteraction();
                }
            }
            line.ChangeState(state);
        }

        if (box == null && boxesCount > 0 && PrefabEmptyBox != null)
        {

            box = Instantiate(PrefabEmptyBox, gameObject.transform);
            float offs = box.GetComponent<SpriteRenderer>().bounds.size.y / 2;
            box.transform.localPosition = new Vector3(0, offs, -1);

        }
    }

    public void OnInteractStop(PlayerController player)
    {
        Debug.Log("StopPring");
        StopPrinting();
        playerToInteract = null;
    }

    public void LoadBox()
    {
        Debug.Log("BoxAdded");
        boxesCount = 1; // LET IT BE SO!!! 
    }

    void StopPrinting()
    {
        Debug.Log("StopPringConfigm");
        isPrinting = false;
        if (PrintClock >= TimeToPrint)
        {
            Debug.Log("Printed");
            GameObject detail = Instantiate(DetailPrefab);
            Draggable draggable = detail.GetComponent<Draggable>();
            playerToInteract.GiveItem(draggable);
            boxesCount -= 1;
            if (boxesCount <= 0)
            {
                Destroy(box);
                box = null;
            }
        }
        Destroy(line);
        Destroy(line.gameObject);
        line = null;
    }
}
