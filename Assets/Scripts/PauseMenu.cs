using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{

    public static bool GameIsPaused = false;
    public GameObject PauseMenuUI;
    public UnityEngine.UI.Button firstButton;

    public static PauseMenu instance;

    public Image guide;

    int paused_by;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        instance = this;
    }

    public void TogglePause()
    {
        if (GameIsPaused) Resume();
        else Pause();
    }


    bool GuideShow = false;
    public void ToggleGuide()
    {
        if (GuideShow) guide.gameObject.SetActive(false);
        else guide.gameObject.SetActive(true);
        GuideShow = !GuideShow;
    }

    public void Resume()
    {
        PauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
        GuideShow = false;
        guide.gameObject.SetActive(false);
    }

    public void Pause()
    {
        PauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
        firstButton.Select();
    }

    public void OnContinuePlayingClick()
    {
        Resume();
    }

    public void OnExitClick()
    {
        Application.Quit();
    }
}
