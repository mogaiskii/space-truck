using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerSelection : MonoBehaviour
{
    PlayerConfig config;
    PlayerManager manager;
    public PlayerController player;

    public GameObject Ready;

    private void Awake()
    {

    }

    private void Start()
    {
        if (manager == null)
        {
            manager = PlayerManager.instance;
        }
        PlayerInput inp = GetComponent<PlayerInput>();
        config = manager.GetConfig(inp);
        config.selector = this;

        switch (config.PlayerIndex)
        {
            case 0:
                transform.position = transform.position + new Vector3(-6, 0, 0);
                break;
            case 1:
                transform.position = transform.position + new Vector3(-2, 0, 0);
                break;
            case 2:
                transform.position = transform.position + new Vector3(2, 0, 0);
                break;
            case 3:
                transform.position = transform.position + new Vector3(6, 0, 0);
                break;
            default:
                break;
        }


        GetComponent<SpriteRenderer>().color = config.color;
        Debug.Log(config.color);
    }

    public void OnActivateChange(InputAction.CallbackContext ctx)
    {
        bool wasIt = ctx.action.triggered;
        if (manager.inGame)
        {
            player?.OnActionChange(ctx);
        }
        else
        {

            if (wasIt)
            {
                manager.SetPlayerReady(config.PlayerIndex);

                Ready.SetActive(true);
            }
        }
    }

    public void OnMove(InputAction.CallbackContext ctx)
    {
        if (manager.inGame)
        {
            player?.OnMoveChange(ctx);
        }
    }

}
